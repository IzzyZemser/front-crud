import React from "react";
import styled from "styled-components";

const CardDiv = styled.div`
  padding: 2rem 2.8rem;
  cursor: pointer;
  display: flex;
  align-items: center;

  :hover {
    background: paleturquoise;
  }
  :hover .thumb {
    box-shadow: 0 0.4rem 1.5rem DimGrey;
    margin-bottom: 3rem;
    padding-bottom: 1rem;
  }
  :active {
    background: skyblue;
    color: white;
  }
  :active .paragraph {
    color: white;
  }
  :active .thumb {
    box-shadow: 0 0.4rem 1.5rem DimGrey;
  }
`;

const TextsBoxDiv = styled.div`
  padding-left: 2.8rem;
`;
const CardTitle = styled.h1`
  font-family: "Expletus Sans";
  text-align: left;
  font-size: 2.8rem;

  font-size: 3.8rem;
  font-weight: 400;
  font-family: "Yanone Kaffeesatz", sans-serif;
  text-align: center;
  color: slategray;
`;

const CardMsgP = styled.p`
  font-family: "Raleway";
  font-size: 1.4rem;
  max-width: 35rem;
`;

const Card = ({ first_name, last_name, email, phone }) => {
  // console.log('first_name:',first_name)
  //	console.log('props:',props)
  return (
    <CardDiv>
      {/* <Thumb image_url={avatar} /> */}
      <TextsBoxDiv>
        <CardTitle>
          {first_name} {last_name}
        </CardTitle>
        <CardMsgP>{`email: ${email}, phone: ${phone}`}</CardMsgP>
      </TextsBoxDiv>
    </CardDiv>
  );
};
export default Card;
