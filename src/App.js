import React, {useEffect, useState } from "react";
import styled from "styled-components";
import List from "./List";
import axios from "axios";

const URL = "http://localhost:3030/api/users/";

const AppDiv = styled.div`
  background: Cornsilk;
  border-radius: 0.4rem;
  display: flex;
  flex-direction: column;
  box-shadow: 0 0.4rem 1.5rem DimGrey;
  position: relative;
  min-width: 60rem;
  height: 100vh;
`;

const ContentDiv = styled.div`
  border-radius: 5px;
  display: flex;
  flex-direction:column;
  justify-content:center;
  
  h1{
    text-align:center;
    font-family: "Yanone Kaffeesatz",sans-serif;
    text-align: center;
    color: slategray;
    margin: 2rem;
    font-size:2rem;
  }
`;

const App = () => {
  const [list, set_list] = useState([]);
  const [is_loading, set_is_loading] = useState(true);
  const [error_msg, set_error_msg] = useState("");



  // async function getData() {
  //   try {
  //     const data_url = "https://api.npoint.io/b8cdd8970e0d9028063a";

  //     const response = await fetch(data_url);
  //     const data = await response.json();
  //     // const data = await (await fetch(data_url)).json();
  //     set_original_list(data);
  //     set_filtered_list(data);
  //     set_profile_data(data[0]);
  //     set_is_loading(false);
  //     set_error_msg("");
  //   } catch (error) {
  //     console.error(`fetch operation failed: ${error.message}`);
  //     set_error_msg("Something went wrong...");
  //   }
  // }

  const getData = async () => {
    try{
      let response = await axios.get("api/users/");    
      set_list(response.data);
      set_is_loading(false);
    }catch(err){
      set_error_msg("Something went wrong...");
    }
   
    
  }

  useEffect(() => getData(), []);

  if (error_msg) return <h1>{error_msg}</h1>;
  return (
    <AppDiv>
      <ContentDiv>
        {is_loading ? (
          <h1>Loading...</h1>
        ) : (
          <>
            <h1>User Data:</h1>
            <List list_data={list} />
          </>
        )}
      </ContentDiv>
    </AppDiv>
  );
};

export default App;
