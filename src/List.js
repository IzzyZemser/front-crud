import React from "react";
import Card from "./Card";
import styled from "styled-components";

const CardListDiv = styled.div`
  background: oldlace;
  height: 75vh;
  border-radius: 0.4rem;
  overflow-x: hidden;
  overflow-y: scroll;
  box-shadow: 0 0.2rem 0.8rem DimGrey;
`;

const LiCardItem = styled.li`
  display:flex;
  justify-content:center;
  :nth-child(even) {
    ackground: honeydew;
  }
  :nth-child(odd) {
    background: white;
  }
`;

const List = ({ list_data }) => {
  return (
    <CardListDiv>
      <ul>
        {list_data?.map((item) => (
          <LiCardItem key={item._id} >
            <Card {...item} />
          </LiCardItem>
        ))}
      </ul>
    </CardListDiv>
  );
};
export default List;

// import React from "react";
// import Card from "./Card";

// const List = ({ list_data, pick }) => (
//   <div className="cards-list">
//     <ul> {create_list_ui(list_data, pick)} </ul>
//   </div>
// );

// const create_list_ui = (items, pick) =>
//   items?.map(item => (
//     <li key={item.id} className="card-item" onClick={() => pick(item)}>
//       <Card {...item} />
//     </li>
//   ));

// export default List;
